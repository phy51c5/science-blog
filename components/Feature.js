import SbEditable from "storyblok-react";

const redirectToBlokContent = async blokName => {
  let blockContentUrl = `https://en.wikipedia.org/w/api.php?action=opensearch&search=${encodeURIComponent(blokName)}&limit=1&namespace=0&format=json`
  window.location.href = blockContentUrl;
  return;
}

const Feature = ({ blok }) => {
  return (
    <SbEditable content={blok}>
      <div onClick={() => redirectToBlokContent(blok.name)} className="py-2 card">
        <h2 className="text-lg"> {blok.name} </h2>
      </div>
      <style jsx>{`
        .card {
          margin: 1rem;
          flex-basis: 45%;
          padding: 1.5rem;
          text-align: left;
          color: inherit;
          text-decoration: none;
          border: 1px solid #eaeaea;
          border-radius: 10px;
          transition: color 0.15s ease, border-color 0.15s ease;
        }
        .card:hover,
        .card:focus,
        .card:active {
          color: #0070f3;
          border-color: #0070f3;
        }
        .card h3 {
          margin: 0 0 1rem 0;
          font-size: 1.5rem;
        }
        .card p {
          margin: 0;
          font-size: 1.25rem;
          line-height: 1.5;
        }
      `}</style>
    </SbEditable>
  );
};

export default Feature;

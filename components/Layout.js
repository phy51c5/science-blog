import React from 'react';
import StoryblokService from '../utils/storyblok-service';
import styles from '../styles/layout.module.css'
import utilStyles from '../styles/utils.module.css'
import Head from 'next/head'
import Link from 'next/link'

const name = 'John Doe';
export const siteTitle = 'Profile page';

const Layout = ({ children }) => (
  <div className={styles.container}>
    <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="Learn how to build a personal website using Next.js"
        />
        <meta
          property="og:image"
          content={`https://og-image.now.sh/${encodeURI(
            siteTitle
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        />
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
        <title>{"Science Blog"}</title>
      </Head>
    <div style={{background: "#eeffdd"}} className="max-w-5xl p-10 mx-auto">
      {children}
      {StoryblokService.bridge()}
    </div>
    <footer>
          <a
            href="https://linux.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Powered by GNU Linux{' '}
            <img src="/linux-logo.png" alt="Linux Logo" className="logo" />
          </a>
          <a
            id="open-source"
            href="#open-source"
          >
            <img src="/open-source.jpg" alt="Open Source" className="logo" />
          </a>
      </footer>
      <style jsx>{`
        a {
          color: inherit;
          text-decoration: none;
        }
        .title a {
          color: #0070f3;
          text-decoration: none;
        }

        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: underline;
        }

        .title {
          margin: 0;
          line-height: 1.15;
          font-size: 4rem;
        }

        .title,
        .description {
          text-align: center;
        }
        footer {
          width: 100%;
          height: 100px;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        footer img {
          margin-left: 0.5rem;
        }

        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
        }
        .logo {
          height: 1.5em;
        }
        .card {
          margin: 1rem;
          flex-basis: 45%;
          padding: 1.5rem;
          text-align: left;
          color: inherit;
          text-decoration: none;
          border: 1px solid #eaeaea;
          border-radius: 10px;
          transition: color 0.15s ease, border-color 0.15s ease;
        }

        .card:hover,
        .card:focus,
        .card:active {
          color: #0070f3;
          border-color: #0070f3;
        }

        .card h3 {
          margin: 0 0 1rem 0;
          font-size: 1.5rem;
        }

        .card p {
          margin: 0;
          font-size: 1.25rem;
          line-height: 1.5;
        }
        .backButton {
          margin : 3rem 64rem 0rem 0rem;
        }
        .poster {
          text-align: center;
        }
        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>
  </div>
)

export default Layout
